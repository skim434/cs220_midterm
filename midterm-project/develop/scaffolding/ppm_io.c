//Sungwon Kim
//skim434
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {


    Image *img = (Image *) malloc(sizeof(Image));
    //This checks if the file format is P6.
    char format1;
    char format2;
    fscanf(fp, "%c", &format1);
    fscanf(fp, "%c ", &format2);
    if(format1 != 'P' || format2 != '6') {
        printf("This is the value of format 1 and format 2 %c, %c", format1, format2);
        fprintf(stderr, "Invalid image format \n");
        exit(1);
    }

    //This gets rid of all hashtags.
    char hashtag = fgetc(fp);
    while (hashtag == '#') {
        while(fgetc(fp) != '\n');
        //printf("Value of hashtag %c", hashtag);
        hashtag = fgetc(fp);
    }
    //printf("Value of hashtag %c", hashtag);
    ungetc(hashtag, fp); //free the last thing read


    //saves the value of rows, cols and check if the 255.
    int checking255;
    int check1 = fscanf(fp, " %d", &(img->cols));
    int check2 = fscanf(fp, " %d", &(img->rows));
    int check3 = fscanf(fp, " %d ", &checking255);
    if((check1 + check2 + check3) != 3) {
        fprintf(stderr, "The number is wd");
        exit(1);
    }
    if(checking255 != 255) {
        fprintf(stderr, "Wrong value of possible rgb values\n");
    }
    // printf("value of rows : %d, valie of column: %d, value of checking255: %d", img->rows, img->cols, checking255);
    //good until here.

    img ->data = (Pixel*) malloc(img->cols * img->rows * sizeof(Pixel));
    fread(img->data, sizeof(Pixel), img->cols * img->rows, fp);
    //fread(img->data, 3*img->cols, img->rows, fp);

    return img;


}
/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}


void swap(Image *img) {
  char rsave;
  char gsave;
  char bsave;

  for (int i = 0; i< ((img ->cols) * (img->rows)); i++) {
	rsave = img ->data[i].r;
	gsave = img->data[i].g;
	bsave = img->data[i].b;
	img->data[i].r = gsave;
	img->data[i].g = bsave;
	img->data[i].b = rsave;
  }
}
  

  
	   
      
	 

int main() {
  FILE *FP = fopen("nika.ppm", "r");
  FILE *orig = fopen("orig.ppm", "w");
  FILE *swapping = fopen("swap.ppm", "w");
  Image * stuff= read_ppm(FP);
  swap(stuff);
  int a = write_ppm(swapping, stuff);
  
  fclose(FP);
  fclose(orig);
  fclose(swapping);
}
