//Sungwon Kim
//skim434
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"

typedef struct {
  int red;
  int green;
  int blue;
} pixel;

typedef struct{
  int x;
  int y;
  pixel *color;

} Image;

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp);

  //checks if the format is P6
  Image *img = (Image *)malloc(sizeof(Image));
  char format1;
  char format2;
  fscanf(fp, "%c", &format1);
  fscanf(fp, "%c ", &format2);
  if(format1 != 'P' || format2 != '6') {
    fprintf(stderr, "Invalid imgae format \n");
    exit(1);
  }


  //get rid of hashtags
  char hashtag = getc(fp);
  while(hashtag == '#') {
    while(getc(fp) != '\n');
    hashtag = getc(fp);
  }
  ungetc(hashtag, fp);


  //read the columns and rows and check 255
  int check1 = fscanf(fp, "%d", &img->x);
  int check2 = fscanf(fp, "%d", &img->y);
  if((check1 + check2) != 2) {
    fprintf(stderr, "The image columns and rows are wrong\n");
    exit(1);
  }
  int pixelRange = 255;
  int pixelCheck;
  fscanf(fp, "%d ", &pixelCheck);
  if(pixelCheck != pixelRange) {
    fprintf(stderr, "The pixel range is wrong");
    exit(1);
  }
  
  img ->color = (pixel*)malloc(check1 * check2 *sizeof(pixel));

  fwrite(img->color, 3 * img->x, img->y, fp);
  
  
  return img;  
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

