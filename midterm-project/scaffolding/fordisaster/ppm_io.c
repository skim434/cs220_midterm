//Sungwon Kim
//skim434
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"
#include <math.h>
/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {


    Image *img = (Image *) malloc(sizeof(Image));
    //This checks if the file format is P6.
    char format1;
    char format2;
    fscanf(fp, "%c", &format1);
    fscanf(fp, "%c ", &format2);
    if(format1 != 'P' || format2 != '6') {
        printf("This is the value of format 1 and format 2 %c, %c", format1, format2);
        fprintf(stderr, "Invalid image format \n");
        exit(1);
    }

    //This gets rid of all hashtags.
    char hashtag = fgetc(fp);
    while (hashtag == '#') {
        while(fgetc(fp) != '\n');
        //printf("Value of hashtag %c", hashtag);
        hashtag = fgetc(fp);
    }
    //printf("Value of hashtag %c", hashtag);
    ungetc(hashtag, fp); //free the last thing read


    //saves the value of rows, cols and check if the 255.
    int checking255;
    int check1 = fscanf(fp, " %d", &(img->cols));
    int check2 = fscanf(fp, " %d", &(img->rows));
    int check3 = fscanf(fp, " %d ", &checking255);
    if((check1 + check2 + check3) != 3) {
      fprintf(stderr, "The number is wierd. values were %d , %d, %d \n", img->cols, img->rows, checking255);
        exit(1);
    }
    if(checking255 != 255) {
        fprintf(stderr, "Wrong value of possible rgb values\n");
    }
    // printf("value of rows : %d, valie of column: %d, value of checking255: %d", img->rows, img->cols, checking255);
    //good until here.

    img ->data = (Pixel*) malloc(img->cols * img->rows * sizeof(Pixel));
    fread(img->data, sizeof(Pixel), img->cols * img->rows, fp);
    //fread(img->data, 3*img->cols, img->rows, fp);

    return img;


}
/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);
  printf("%d , %d", im->cols, im->rows);
  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}


/*swaps the rgb value once. green value becomes red, blue value becomes green.
 * and red value becomes blue.
 */

void swap(Image *img) {
  char rsave;
  char gsave;
  char bsave;

  for (int i = 0; i< ((img ->cols) * (img->rows)); i++) {
	rsave = img ->data[i].r;
	gsave = img->data[i].g;
	bsave = img->data[i].b;
	img->data[i].r = gsave;
	img->data[i].g = bsave;
	img->data[i].b = rsave;
  }
}

// swaps the color a times
void swapNum(Image *img, int a) {
  if(a<0) {
    printf("This is not a valid number of times to swap.");
  }
  
  for (int i = 0; i<a; i++) {
    swap(img);
  }

}


unsigned char addPixel(unsigned char val, double change);
unsigned char intensity(unsigned char r, unsigned char g, unsigned char b);

void brightness(Image *img, double change) {

  for (int i = 0; i<((img ->cols) * (img ->rows)); i++) {
    img->data[i].r = addPixel(img ->data[i].r, change);
    img->data[i].g = addPixel(img ->data[i].g, change);
    img->data[i].b = addPixel(img ->data[i].b, change); 
    
   } 

}


void invert(Image *img) {
  for (int i = 0; i <((img ->cols) * (img ->rows)); i++) {
    img->data[i].r = 255-img->data[i].r;
    img->data[i].g = 255-img->data[i].g;
    img->data[i].b = 255-img->data[i].b;
  }

}

void grayscale(Image *img) {
 for (int i = 0; i <((img ->cols) * (img ->rows)); i++) {
   unsigned char inten =intensity(img ->data[i].r, img->data[i].g, img->data[i].b);
   img->data[i].r = inten;
   img->data[i].g = inten;
   img->data[i].b = inten;
  }
  
}


void crop(Image *img, int topc, int topr, int botc, int botr) {

  //changing the dimension of the image.
  int savecol = img->cols;
  Pixel *freethis = img->data;
  img->rows = botr-topr;
  img->cols = botc-topc;
  int space = 0;
  Pixel *copy;
  copy = (Pixel*) malloc(img->cols * img->rows * sizeof(Pixel));
    //check of topr and botr range is right.
  for (int i =topr; i<botr; i++) {
    for (int k = savecol*i+topc; k<savecol*i+botc; k++) {
      copy[space].r = img->data[k].r;
      copy[space].g = img->data[k].g;
      copy[space].b = img->data[k].b;
      space++;
    }
  }
  img->data = copy;
  printf("number of rows is %d\n", img->rows);
  printf("number of cols is %d\n", img->cols);
  free(freethis);
}

void copyTest(Image *img) {
  int space = 0;
  Pixel *copy;
  copy = (Pixel*) malloc(img->cols * img->rows * sizeof(Pixel));
    //check of topr and botr range is right.
  for (int i =0; i<(img->rows); i++) {
    for (int k = 0; k< (img->cols); k++) {
      copy[space].r = img->data[space].r;
      copy[space].g = img->data[space].g;
      copy[space].b = img->data[space].b;
      space++;
    }
  }
  img->data = copy;
  printf("number of rows is %d\n", img->rows);
  printf("number of cols is %d\n", img->cols);
}



//returns 0 if the columns and rows are not in the range.
//returns 1 if the columns and rows are in the range.
int inImage(Image *img, int col, int row) {

  //check if the range is negative.
  if((col <0) || (row <0)) return 0;

  //check if the row is correct.
  if (img->rows <row) return 0;

  if (img->cols <col) return 0;

  return 1;
}



double sq(double a) {

    return a*a;

}
double gaussdis(int dx, int dy, double sigma) {
  double M_PI = 3.141592653589793238463;
  double g = (1.0 / (2.0 * M_PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
  return g;


}


//returns a gaussian Matrix of size a.(a odd)
double * gaussMatrix(int a, double sigma) {
  if(a%2 == 0) {
     printf("This is not a valid value for gausian blur"); 
   } 
   double *out = (double *)malloc(a*a*sizeof(double)); 
   int dx = 0; 
   int dy = 0; 
   for (int i = 0; i<a*a; i++) { 
     dx =(a-1)/2 -  i%a; 
     dy =(a-1)/2 -  i/a; 
     out[i] = gaussdis(dx, dy, sigma); 
   } 
  
 } 
 int doubleToSingle(int r, int c, int colNum) { 
   return r*colNum + c; 
 } 

 void gaussMultiple(Image *img, double *gaussMatrix,int size,  int r, int c) { 
   double rval= 0; 
   double gval = 0;
   double bval = 0; 
   int difx; 
   int dify; 
   int multiply; 
   int rcopy = r; 
   int ccopy = c; 
   int placeInIm;
   double filadd=0;
   for (int i = 0; i<size*size; i++) { 
     difx =(i%size)-(size-1)/2; 
     dify =(i/size) - (size-1)/2; 
     ccopy +=difx; 
     rcopy +=dify; 
     multiply = inImage(img, ccopy, rcopy); 
     placeInIm = ccopy + rcopy*(img ->cols); 

     //typecast
     double rd = (double) img->data[placeInIm].r; 
    double gd = (double) img->data[placeInIm].g; 
    double bd = (double) img->data[placeInIm].b;
    
    rval += multiply*gaussMatrix[i]*rd;
    gval += multiply*gaussMatrix[i]*gd;
    bval += multiply*gaussMatrix[i]*bd;
    filadd += multiply*gaussMatrix[i];
    rcopy = r;
    ccopy = c;
  }
   int value = doubleToSingle(r,c, img->cols);
  img->data[value].r = (unsigned char) rval/filadd;
  img->data[value].g = (unsigned char) gval/filadd;
  img->data[value].b = (unsigned char) bval/filadd;

}


void  gauss(Image *img, int size, double sigma) {

  //making a deep copy of the pixel values
  Pixel *copy = (Pixel*)malloc(sizeof(img->data));
  double *gaussian = gaussMatrix(size, sigma);
  
  for (int i =0; i<(img->rows * img->cols); i++) {
    copy[i].r = img->data[i].r;
    copy[i].g = img->data[i].g;
    copy[i].b = img->data[i].b;
  }

  
  int r;
  int c;
  for (int i =0; i<(img->rows * img->cols); i++) {
    r = i/(img->cols);
    c = i%(img->cols);
    gaussMultiple(img, gaussian,size,  r , c);
  }
 
}






unsigned char addPixel(unsigned char val, double change) {
  double k = val + change;
  if(k > 255) {
    k = 255;
  }
  else if (k <0) {
    k = 0;
  }
  k = (unsigned char) k;
  return k;
}


unsigned char intensity(unsigned char r, unsigned char g, unsigned char b) {
  return (unsigned char) 0.3*r+0.59*g+0.11*b;
}


	   
      
	 

int main() {
  FILE *FP = fopen("nika.ppm", "r");
  Image *b = read_ppm(FP);
  FILE *orig = fopen("orig.ppm", "w");
  FILE *swapping = fopen("swap.ppm", "w");
  FILE *bright = fopen("bright.ppm", "w");
  FILE *cropping = fopen("crop.ppm", "w");
  FILE *grayscaletest = fopen("greyscale.ppm", "w");
  FILE *copyTesting = fopen("copytests.ppm", "w");
  FILE *gaussianBlur = fopen("gaussian.ppm", "w");
  //copyTest(b);
  //write_ppm(copyTesting, b);
  gauss(b, 11, 1.0);
  write_ppm(gaussianBlur, b);
   
  
  fclose(FP);
  fclose(orig);
  fclose(swapping);
  fclose(cropping);
}
