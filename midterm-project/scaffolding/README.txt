//skim434
//Sungwon Kim


This is the README text.

The basic design of the program is as follows: The main section of project.c first checks if the read folder and the write folder is valid. Then it calls the ppm_io.c file to read in the ppm file to image.
Then the project.c file checks if the requirements for correct input is satisfied and calls functions from imageManip.c . The manipulated image then is written by ppm_io.c file and all streams are closed.

Made deep copy for blur and edges. for crop, made a pixel pointer and swapped the data value.

Known possible error: If the output file is not a ppm file, the program still treats it like a ppm file. Was not sure if we should consider this error.

All functions till swap produced exact output.
