//Sungwon Kim
//skim434

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>

//swaps the color. red becomes blue, green becomes red, and blue becomes green.
void swap(Image *img);

//calls swap function "a" times.
void swapNum(Image *img, int a);

//returns 1 if the ppm files are identical. 0 otherwise.
int sameImg(Image *img, Image *compare);

//helper function for adding brightness to a pixel.
unsigned char addPixel(unsigned char val, double change);


//helper function for grayscale.
unsigned char intensity(unsigned char r, unsigned char g, unsigned char b);


//changes the brightness of all pixels in the image.
void brightness(Image *img, double change);

//inverts the brightness of all pixels.
void invert(Image *img);

//makes image grayscale
void grayscale(Image *img);


//crops an image according to following dimensions
void crop(Image *img, int topc, int topr, int botc, int botr);


//returns 1 if the col and row are inside the image. 0 if not.(Used for blur)
int inImage(Image *img, int col, int row);

//squares the value a
double sq(double a);

//returns a gaussian distribution value according to how far the pixel is.
double gaussdis(int dx, int dy, double sigma);

//returns the size of the matrix according to sigma
int matrixSize(double sigma);

//makes gaussian matrix
double * gaussMatrix(double sigma);

//gives row position of 1d pos.
int twodr(int pos, int colLen);

//gives col position of 1d pos
int twodc(int pos, int colLen);

//changes the col and row value to 1d array value.
int doubleToSingle(int r, int c, int colNum);

//returns the character value for red pixel on a given position.
unsigned char gaussianMultipleR(Image *img, int pos, double *gaussMatrix, double sigma);

//returns the character value for blue pixel on a given position
unsigned char gaussianMultipleG(Image *img, int pos, double *gaussMatrix, double sigma);

//gives the character value for green pixel on a given position
unsigned char gaussianMultipleB(Image *img, int pos, double *gaussMatrix, double sigma);

//does blur function on the image
void gaussianBlur(Image *img, double sigma);

//returns the pointer value of a deep copy
Image * dImageCopy(Image *img);

//returns the intensity gradient of the x direction
double xchange(Image *img, int pos);

//returns the intensity gradient of the y direction
double ychange(Image *img, int pos);

//calculate the magnitude of intensity gradient
double magnitude(Image *img, int pos);

//returns 0 if the mag is smaller than the threshold 1 otherwise
int isBound(Image *img, int pos);

//manipulate image for edges function
void edge(Image *img, double sigma, int thres);

//void gaussMultiple(Image *img, double *gaussMatrix, int size, int r, int c);

//void gauss(Image *img, int size, double sigma);

