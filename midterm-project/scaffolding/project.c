//skim434
//Sungwon Kim

#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "ppm_io.h"
#include "imageManip.h"
#include <ctype.h>

//sees if the coordinates are in range of the image.
int nDim(Image *img, int c, int r) {
  if(c < 0 || r < 0) return 0;
  if (img->cols >= c && img->rows >= r) return 1;
  return 0;
}


//checks if the input is a number.
int check(char* s){
  for(int i = 0; i < (int) strlen(s); i++){
    if(isdigit(s[i]) == 0)
      return 0;
  }
  return 1;
}


int main(int argc, char* argv[]) {
  int execution = 0; //number of execution of functions

  //printf self explanatory). Similarly, all the error checking if statements have a self-explanatory printf statements.
   if(argc < 3){
    printf("Failed to supply input filename or output filename, or both\n");
    return 1;
  }
   
   if(argc ==3) {
     printf("No operation name was specified, or operation name specified was invalid\n");
     return 4;
   } 
      
 
  FILE *FP = fopen(argv[1], "r");
  
  if(FP == NULL){
    printf("Specified input file could not be opened\n");
    // no  fclose(FP) since it is null;
    return 2;
  }

  
  Image *b = read_ppm(FP);

  if(b == NULL) {
    printf("The read file is somewhat wrong.\n");
    return 3;
  }
  FILE *savePlace = fopen(argv[2], "w");

  

  
  if(strcmp(argv[3],"swap") == 0) {
    if(argc != 4) {
      printf("The number of arguments is wrong\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    
    swap(b);
    execution++;
  }
  
  else if(strcmp(argv[3],"bright") == 0) {
    if(argc != 5){
      printf("The number of arguments is wrong\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    brightness(b, atoi(argv[4]));
    execution++;
  }

  else if (strcmp(argv[3],"invert")==0) {
    
    if(argc != 4) {
      printf("The number of arguments is wrong\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    
    invert(b);
    execution++;
  }
  
  else if (strcmp(argv[3],"gray")==0) {
    if(argc != 4) {
      printf("The number of arguments is wrong\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    
    grayscale(b);
    execution++;
  }
  else if (strcmp(argv[3],"crop")== 0) {
    if(argc != 8){
      printf("The number of arguments is wrong\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    
    if(!(check(argv[4]) || check(argv[5]) || check(argv[6]) || check(argv[7]))){
      printf("Check if the cropping places are all integers\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
      }

    //checks if input is float(not needed)
    if(!(fabs(atof(argv[4]) - atoi(argv[4])) < 0.000001 || fabs(atof(argv[5]) - atoi(argv[5])) < 0.000001 || fabs(atof(argv[6]) - atoi(argv[6])) < 0.000001 || fabs(atof(argv[7]) - atoi(argv[7])) < 0.000001)){
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      printf("Check if the cropping places are all integers\n");
	return 5;
      }
    
    if(!(nDim(b, atoi(argv[4]), atoi(argv[5])))){
      printf("Crop range is not on Image\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 6;
    }
    
    if(!(nDim(b, atoi(argv[6]), atoi(argv[7])))){
      printf("Crop range is not on Image\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 6;
    }
    
    if(atoi(argv[6]) - atoi(argv[4]) <= 0 ||  atoi(argv[7]) - atoi(argv[5]) <= 0){
      printf("The range of bottom row column is smaller or equal to the top\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 6;
    }
    crop(b, atoi(argv[4]),atoi(argv[5]) , atoi(argv[6]), atoi(argv[7]));
    
    execution++;
  }
  else if (strcmp(argv[3], "blur") == 0) {
    if(argc != 5){
      printf("The number of arguments is wrong");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    if(!check(argv[4])) return 5;
    gaussianBlur(b, atof(argv[4]));
    execution++;		 
  }

  else if (strcmp(argv[3], "edges") == 0) {
    if(argc != 6){
      printf("Invalid number of arguments\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    if(!(check(argv[4]) || check(argv[5]))) {
      printf("Check if the sigma and the gradient is a number\n");
      fclose(FP);
      fclose(savePlace);
      free(b->data);
      free(b);
      return 5;
    }
    edge(b, atof(argv[4]), atoi(argv[5]));
    execution++;
  }
  if (execution ==0) {
    printf("No operation name was specified, or operation name specified was invalid\n");
    return 4;
  }
  
  write_ppm(savePlace, b);
  
  free(b->data);
  free(b);
  fclose(FP);
  fclose(savePlace);
  
  return 0;
}


