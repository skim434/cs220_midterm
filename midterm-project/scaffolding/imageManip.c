//skim434
//Sungwon Kim

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"
#include <math.h>



//swaps the image coloing
void swap(Image *img) {
  char rsave;
  char gsave;
  char bsave;

  for (int i = 0; i< ((img ->cols) * (img->rows)); i++) {
        rsave = img ->data[i].r;
        gsave = img->data[i].g;
        bsave = img->data[i].b;
        img->data[i].r = gsave;
        img->data[i].g = bsave;
        img->data[i].b = rsave;
  }
}

// swaps the color 'a' times. (multiple)
void swapNum(Image *img, int a) {
  if(a<0) {
    printf("This is not a valid number of times to swap.");
  }

  for (int i = 0; i<a; i++) {
    swap(img);
  }

}

int sameImg(Image *img, Image *compare) {
  if(img->rows != compare->rows) return 0;
  if(img->cols != compare->cols) return 0;
  for (int i = 0; i<img->rows * img->cols; i++) {
    if(img->data[i].r != compare->data[i].r) return 0;
    if(img->data[i].g != compare->data[i].g) return 0;
    if(img->data[i].b != compare->data[i].b) return 0;
  }
  return 1;
}

unsigned char addPixel(unsigned char val, double change);
unsigned char intensity(unsigned char r, unsigned char g, unsigned char b);

void brightness(Image *img, double change) {

  for (int i = 0; i<((img ->cols) * (img ->rows)); i++) {
    img->data[i].r = addPixel(img ->data[i].r, change);
    img->data[i].g = addPixel(img ->data[i].g, change);
    img->data[i].b = addPixel(img ->data[i].b, change);

   }

}


void invert(Image *img) {
  for (int i = 0; i <((img ->cols) * (img ->rows)); i++) {
    img->data[i].r = 255-(img->data[i].r);
    img->data[i].g = 255-(img->data[i].g);
    img->data[i].b = 255-(img->data[i].b);
  }

}


unsigned char intensity(unsigned char r, unsigned char g, unsigned char b) {

  return (unsigned char) (0.3*r + 0.59*g + 0.11*b);

}
void grayscale(Image *img) {
 for (int i = 0; i <((img ->cols) * (img ->rows)); i++) {
   unsigned char inten =intensity(img ->data[i].r, img->data[i].g, img->data\
[i].b);
   img->data[i].r = inten;
   img->data[i].g = inten;
   img->data[i].b = inten;
  }

}


void crop(Image *img, int topc, int topr, int botc, int botr) {

  //changing the dimension of the image.
  int savecol = img->cols;
  Pixel *freethis = img->data;
  img->rows = botr-topr;
  img->cols = botc-topc;
  int space = 0;
  Pixel *copy;
  copy = (Pixel*) malloc(img->cols * img->rows * sizeof(Pixel));
    //check of topr and botr range is right.
  for (int i =topr; i<botr; i++) {
    for (int k = savecol*i+topc; k<savecol*i+botc; k++) {
      copy[space].r = img->data[k].r;
      copy[space].g = img->data[k].g;
      copy[space].b = img->data[k].b;
      space++;
    }
  }
  img->data = copy;
  // printf("number of rows is %d\n", img->rows);
  //printf("number of cols is %d\n", img->cols);
  free(freethis);
}


int inImage(Image *img, int col, int row) {

  //check if the range is negative.
  if((col <0) || (row <0)) return 0;

  //check if the row is correct.
  if (img->rows <= row)return 0;

  if (img->cols <= col) return 0;

  return 1;
}

double sq(double a) {

    return a*a;

}
double gaussdis(int dx, int dy, double sigma) {
  double PI = 3.141592653589793238463;
  //printf("This is the value of sigma %f\n", sigma);
  double g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
  // printf("This is the value of gaussdis. %f\n", g);
  return g;


}


int matrixSize(double sigma) {
    int out = (int) ceil(sigma*10);
    if(out%2 == 0){
        out++;
    }
    return out;
}


//returns a gaussian Matrix of size a.(a odd)
double * gaussMatrix(double sigma) {
  int a = matrixSize(sigma);
   double *out = (double *)malloc(a*a*sizeof(double));
   int dx = 0;
   int dy = 0;
   int counter = 0;
   for (int i = 0; i<a*a; i++) {
     dx = i%a-(a-1)/2;
     dy = i/a-(a-1)/2;
     out[i] = gaussdis(dx, dy, sigma);
      counter = counter+1;
   }
  
   return out;
 }

//finds the row value of one d matrix.
int twodr(int pos, int colLen) {
  return pos/colLen;
}


//finds the col value of one d matrix
int twodc(int pos, int colLen) {
  return pos%colLen;
}


int doubleToSingle(int r, int c, int colNum) {
   return r*colNum + c;
}

/*unsigned char  gaussianMultipleR(Image *img, int pos, double *gaussMatrix, double sigma){
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  int rcopy = rval;
  int ccopy = cval;
  int size = matrixSize(sigma);
  int dx;
  int dy;
  double gaussSum= 0 ;
  double weightSum = 0;
  for(int i = 0; i<size * size; i++) {
    dx = i%size - (size-1)/2;
    dy = i/size - (size-1)/2;
    cval = cval +dx;
    rval = rval+ dy;
    if(inImage(img, cval, rval)){
      int location = doubleToSingle(rval, cval, img->cols);
      gaussSum += ((double) (img->data[location].r)) * gaussMatrix[i];
      weightSum += gaussMatrix[i];
    }
    //initialize cval, rval again
    rval = rcopy;
    cval = ccopy;
  }
  //("This is the value of value %f, gaussSum, weightSum);
  return (unsigned char)(gaussSum/weightSum) ;
}
*/


unsigned char  gaussianMultipleR(Image *img, int pos, double *gaussMatrix, double sigma){
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  int rcopy = rval;
  int ccopy = cval;
  
  int size = matrixSize(sigma);
  int dx;
  int dy;
  double gaussSum=0;
  double weightSum=0;
  for(int i = 0; i<size * size; i++) {
    dx = i%size - (size-1)/2;
    dy = i/size - (size-1)/2;
    cval = cval + dx;
    rval = rval + dy;
    // printf("The value of the first sum is %f", img->data[0].g *0.01);
    if(inImage(img, cval, rval)){
      int location = doubleToSingle(rval, cval, img->cols);
      gaussSum += (double)((img->data[location].r)) * gaussMatrix[i];
      //printf("GaussSum %f\n", gaussMatrix[i]);
      weightSum += gaussMatrix[i];
    }
    //initialize cval, rval again
    rval = rcopy;
    cval = ccopy;
  }
  // printf("the value of rval %d\n", rval);
  //printf("the value of gaussSum %f\n", gaussSum);
  return (unsigned char) (gaussSum/weightSum);
}

unsigned char  gaussianMultipleG(Image *img, int pos, double *gaussMatrix, double sigma){
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  int rcopy = rval;
  int ccopy = cval;
  
  int size = matrixSize(sigma);
  int dx;
  int dy;
  double gaussSum=0;
  double weightSum=0;
  for(int i = 0; i<size * size; i++) {
    dx = i%size - (size-1)/2;
    dy = i/size - (size-1)/2;
    cval = cval +dx;
    rval = rval+ dy;
    // printf("The value of the first sum is %f", img->data[0].g *0.01);
    if(inImage(img, cval, rval)){
      int location = doubleToSingle(rval, cval, img->cols);
      gaussSum += (double)((img->data[location].g)) * gaussMatrix[i];
      //printf("GaussSum %f\n", gaussMatrix[i]);
      weightSum += gaussMatrix[i];
    }
    //initialize cval, rval again
    rval = rcopy;
    cval = ccopy;
  }
  // printf("the value of rval %d\n", rval);
  //printf("the value of gaussSum %f\n", gaussSum);
  return (unsigned char) (gaussSum/weightSum) ;
}

unsigned char  gaussianMultipleB(Image *img, int pos, double *gaussMatrix, double sigma){
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  int rcopy = rval;
  int ccopy = cval;
  
  int size = matrixSize(sigma);
  int dx;
  int dy;
  double gaussSum = 0;
  double weightSum = 0;
  for(int i = 0; i<size * size; i++) {
    dx = i%size - (size-1)/2;
    dy = i/size - (size-1)/2;
    cval = cval +dx;
    rval = rval+ dy;
    if(inImage(img, cval, rval)){
      int location = doubleToSingle(rval, cval, img->cols);
      gaussSum += (double)((img->data[location].b)) * gaussMatrix[i];
      weightSum += gaussMatrix[i];
    }
    //initialize cval, rval again
    rval = rcopy;
    cval = ccopy;
  }
  return  (unsigned char) (gaussSum/weightSum) ;
}

void gaussianBlur(Image *img, double sigma){
  double * gaussM = gaussMatrix(sigma);
  //making a deep copy of the pixel values
  Image *dcopy = (Image *) malloc(sizeof(Image));
  dcopy->cols = img->cols;
  dcopy->rows = img->rows;
				  
  dcopy->data = (Pixel*)malloc(dcopy->cols * dcopy->rows *sizeof(Pixel));
  for (int i =0; i<(img->rows * img->cols); i++) {
    dcopy->data[i].r = img->data[i].r;
    dcopy->data[i].g = img->data[i].g;
    dcopy->data[i].b = img->data[i].b;
  }
 
  int counter = 0;
  for (int i  = 0; i<(img->cols) * (img->rows); i++) {
    img->data[i].r = gaussianMultipleR(dcopy, i, gaussM, sigma);
    img->data[i].g = gaussianMultipleG(dcopy, i, gaussM, sigma);
    img->data[i].b = gaussianMultipleB(dcopy, i, gaussM, sigma);
    counter++;
    // printf("This is the value of pixel value %u", img->data[i].r);
    
  }
  free(dcopy->data);
  free(dcopy);
  free(gaussM);
  
  

}


Image * dImageCopy(Image *img) {
  Image *dcopy = (Image *) malloc(sizeof(Image));
  dcopy->cols = img->cols;
  dcopy->rows = img->rows;
				  
  dcopy->data = (Pixel*)malloc(dcopy->cols * dcopy->rows *sizeof(Pixel));
  for (int i =0; i<(img->rows * img->cols); i++) {
    dcopy->data[i].r = img->data[i].r;
    dcopy->data[i].g = img->data[i].g;
    dcopy->data[i].b= img->data[i].b;
  }
  return dcopy;

}

double xchange(Image *img, int pos) {
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  int posval = doubleToSingle(rval, cval+1, img->cols);
  int minval = doubleToSingle(rval, cval-1, img->cols);
  return (img->data[posval].r-img->data[minval].r)/2.0;

}


double ychange(Image *img, int pos) {
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  int posval = doubleToSingle(rval+1, cval, img->cols);
  int minval = doubleToSingle(rval-1,cval, img->cols);
  return (img->data[posval].r-img->data[minval].r)/2.0;

}


double magnitude(Image *img, int pos) {
  return sqrt(sq(xchange(img, pos)) + sq(ychange(img, pos)));
}

int isBound(Image *img, int pos) {
  int rval = twodr(pos, img->cols);
  int cval = twodc(pos, img->cols);
  if ((rval+1)== img->rows) return 0;
  if(rval == 0) return 0;
  if(cval == 0) return 0;
  if(cval+1 == img->rows) return 0;
  return 1;

}
    
    
void edge(Image *img, double sigma, int thres) {

  grayscale(img);
  gaussianBlur(img, sigma);
  
  Image *dcopy = dImageCopy(img);
  for (int i=0; i<(img->rows * img->cols); i++) {
    if(isBound(img, i)){
    double mag = magnitude(dcopy, i);
    if(mag > thres) {
      img->data[i].r = 0;
      img->data[i].g = 0;
      img->data[i].b = 0;
    }
    else {
     img->data[i].r = 255;
     img->data[i].g = 255;
     img->data[i].b = 255;
    } 
    }
    
  }
  free(dcopy->data);
  free(dcopy);


}
//Image *prepEdge(Image *img, double sigma, int thres) {



//}
/*
void gaussMultiple(Image *img, double *gaussMatrix,int size,  int r, int c) {
   double rval= 0;
   double gval = 0;
   double bval = 0;
   int difx;
   int dify;
   int multiply;
   int rcopy = r;
   int ccopy = c;
   int placeInIm;
   double filadd=0;
   for (int i = 0; i<size*size; i++) {
     difx =(i%size)-(size-1)/2;
     dify =(i/size) - (size-1)/2;
     ccopy +=difx;
     rcopy +=dify;
     multiply = inImage(img, ccopy, rcopy);
     placeInIm = ccopy + rcopy*(img ->cols);

     //typecast
   double rd = (double) img->data[placeInIm].r;
   double gd = (double) img->data[placeInIm].g;
   double bd = (double) img->data[placeInIm].b;

    rval += multiply*gaussMatrix[i]*rd;
    gval += multiply*gaussMatrix[i]*gd;
    bval += multiply*gaussMatrix[i]*bd;
    filadd += multiply*gaussMatrix[i];
    rcopy = r;
    ccopy = c;
  }
   int value = doubleToSingle(r,c, img->cols);
  img->data[value].r = (unsigned char) rval/filadd;
  img->data[value].g = (unsigned char) gval/filadd;
  img->data[value].b = (unsigned char) bval/filadd;

}


void  gauss(Image *img, int size, double sigma) {

  //making a deep copy of the pixel values
  Pixel *copy = (Pixel*)malloc(sizeof(img->data));
  double *gaussian = gaussMatrix(size, sigma);

  for (int i =0; i<(img->rows * img->cols); i++) {
    copy[i].r = img->data[i].r;
    copy[i].g = img->data[i].g;
    copy[i].b = img->data[i].b;
  }


  int r;
  int c;
  for (int i =0; i<(img->rows * img->cols); i++) {
    r = i/(img->cols);
    c = i%(img->cols);
    gaussMultiple(img, gaussian,size,  r , c);
  }

}
*/


unsigned char addPixel(unsigned char val, double change) {
  double k = val + change;
  if(k > 255) {
    k = 255;
  }
  else if (k <0) {
    k = 0;
  }
  k = (unsigned char) k;
  return k;
}


